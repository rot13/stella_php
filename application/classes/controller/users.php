<?php
/*
 * users.php
 * 
 * Copyright 2012 Kevin Wolny <kevin.wolny@gmail.com>
 * 
 */

class Controller_Users extends Controller_Default{
	/**
	 * @var array Dane usera z modelu
	 */
	protected $user_data;
	
	public function action_index(){
		$user= new Model_Users;
		$id = $this->request->param('id');
		$this->user_data = $user->get_preview($id);
		if (is_numeric($this->user_data)){
			$this->request->redirect('/');
		}
		$this->view->title='Profil '.$this->user_data['name'].' '.$this->user_data['surname'].' - '.$this->system_name_version;
		$this->view->body = View::factory('user/preview');
		$this->view->body->data=$this->user_data;
		$this->view->body1 = View::factory('user/stream');
		$this->view->body1->stream = $user->get_stream($id);
		$this->view->body.= $this->view->body1;
	}
	public function action_gab(){
		$this->view->body='dajemy tu galbote usera (id:'.$this->request->param('id').')';
	}
	public function action_register(){
		if (isset($_POST['mail'])){
			$reg = Validation::factory($_POST)
				->rule('mail', 		'not_empty')
				->rule('name', 		'not_empty')
				->rule('surname', 	'not_empty')
				->rule('pwd', 		'not_empty')
				->rule('pwd2', 		'not_empty')
				
				->rule('pwd',		'min_length', array(':value',6))
				->rule('pwd2',		'min_length', array(':value',6))
				
				->rule('pwd', 		'max_length', array(':value',30))
				->rule('pwd2', 		'max_length', array(':value',30))
				
				->rule('mail', 		'email', array(':value'))
				
				->rule('name', 		'alpha', array(':value',TRUE))//UTF8 ON
				->rule('surname', 	'alpha', array(':value',TRUE))//UTF8 ON
				->rule('pwd',		'alpha_dash', array(':value',FALSE))//UTF8 OFF
				->rule('pwd2',		'alpha_dash', array(':value',FALSE))//UTF8 OFF
				
				->rule('pwd',		'matches', array(':validation','pwd','pwd2'));
				
			if ($reg->check()){
				$user = new Model_Users;
				$resp = $user->register_user($reg->data());
				if ($resp===1){
					$this->view->body = 'alles git';
					$this->request->redirect('/user/register/after');
				}
				else{
					$this->view->body = 'Wyjebało błędy takie: '.$resp;
				}
			}
			else{
				$errors = $reg->errors('users');
				$this->view->body='<p class="message">Some errors were encountered, please check the details you entered.</p><ul class="errors">';
				foreach ($errors as $message){
					$this->view->body .='<li>'.$message.'</li>';
				}
				$this->view->body.='</ul>';
			}
			
			
			//$reg=$_POST;
			//$this->view->body = $reg['mail'].' '.$reg['name'];
		}
		else{
			$this->view->title = 'Rejestracja - '.$this->system_name_version;
			$this->view->body = View::factory('user/register');
		}
	}
	
	public function action_register_after(){
		$this->view->body = 'ok, Sprawdź swojego maila, dostałeś(aś) linkacza aktywacyjnego';
	}
	
	public function action_login(){
		$id	= $this->session->get('user_id', NULL);
		//if ($id===NULL){//jeśli nie jesteś już zalogowany
		if ($this->check_access(-1)){
			if ($_POST){//jeśli są dane do zalogowania
				$user = new Model_Users;
				$id = $user->login_user($_POST['mail'],$_POST['pwd']);
				if ($id['id']!==0){
					$this->session->set('user_id', $id['id']);
					$this->session->set('user_name', $id['name']);
					$this->session->set('user_surname', $id['surname']);
					$this->request->redirect('/');
				}
				else{
					$this->view->body = 'Bad pwd or email';
				}
			}
			else{
				$this->view->body = View::factory('user/login');
			}
		}
		else{//jeśli jesteś już zalogowany to odsyłam na główną
			$this->request->redirect('/');
		}
	}
	
	public function action_logout(){
		$this->session->delete('user_id');
		$this->request->redirect('/');
	}
	public function action_stream(){
		$user = new Model_Users;
		$id	= $this->request->param('id',NULL);
		if ($id === NULL){
			$this->request->redirect('/');
		}
		$data = $user->get_tweet($id);
		//die(var_dump($data));
		if (is_int($data)){
			$this->request->redirect('/user/stream/'.$data);
		}
		else{
			$this->view->body = View::factory('user/tweet');
			$this->view->body->data = $data;
		}
		/*$stream = $user->get_stream($this->request->param('id'));
		$this->view->body = View::factory('user/stream');
		$this->view->body->stream = $stream;
		*/
		
	}
	public function action_add_tweet(){
		if ($this->check_access(1)){//jeśli jest zalogowany
			if (isset($_POST['text'])){
				$reg = Validation::factory($_POST)
				->rule('text','not_empty')
				//->rule('text','alpha_dash',array(':value',FALSE))
				->rule('text','max_length',array(':value',140));
				if ($reg->check()){
					$user = new Model_Users;
					$tweet = $reg->data();
					$tweet['id_autor'] = $this->session->get('user_id',NULL);
					if ($user->add_tweet($tweet)){
						$this->view->body = "Tweet dodany pomyślnie!";
					}
					else {
						$this->view->body = "Błąd przy dodawaniu tweeta!";
					}
				}
				else{
					$errors = $reg->errors('users');
					$this->view->body='<p class="message">Some errors were encountered, please check the details you entered.</p><ul class="errors">';
					foreach ($errors as $message){
						$this->view->body .='<li>'.$message.'</li>';
					}
					$this->view->body.='</ul>';
					//$this->view->body = "wypełnij pole?!";
				}
			}
			else{
				$this->request->redirect('/');
			}
		}
		else{
			$this->request->redirect('/');
		}
	}
	public function action_cockpit(){
		if ($this->check_access(1)){
			$this->view->body = View::factory('user/add_tweet');
		}
		else{
			$this->view->body = 'Musisz się zalogować!';
		}
	}
}
?>
