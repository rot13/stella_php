<?php
/*
 * default.php
 * 
 * Copyright 2012 Kevin Wolny <kevin.wolny@gmail.com>
 * 
 */
defined('SYSPATH') or die('No direct script access.');
/**
 *  Default controller class for templating.
 *
 * @package    Controller_Default
 * @category   Controller
 * @author     rot13
 * @copyright  GNU General Public License
 * @license	   http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Controller_Default extends Controller {
	/**
	 * @var View
	 */
	public $view;
	
	/**
	 * @var string 	Version e.g. '0.1.2'
	 */
	public $ver;
	
	/**
	 * @var string	App name in title bar and footer
	 */
	public $system_name;
	
	/**
	 * @var string App name + version numer
	 */
	public $system_name_version;
	
	protected $session;
	
	protected $user_access_lvl;
	
	protected $user_id;
	
	
	
	public function before(){
		//dzieje się przed wczytaniem kontrolera
		parent::before();
		
		$this->session = Session::instance();
		//$this->session = 
		$this->user_id = $this->session->get('user_id', NULL);
		$this->user_access_lvl = $this->session->get('user_access_lvl', 0);
		/*if ($this->user_id!==NULL){
			$loggon = 'Zalogowany $id:'.$this->user_id;
		}
		else{
			$loggon = 'Chuj, nie zalogowany';
		}*/

		$this->system_name = 'Stella';
		$this->ver = '0.1';
		$this->system_name_version = $this->system_name.' '.$this->ver;
		
		$this->view = View::factory('default/index');
		$this->view->title = $this->system_name_version;
		$this->view->body = 'body strony defaultowej';
		$this->view->snv = $this->system_name_version;
		$this->view->styles = array(
			'media/css/default.css',
			'media/css/foundation.min.css',
			'media/css/app.css'
		);
		$this->view->scripts = array(
			'media/js/modernizr.foundation.js'
		);
		
	}
	public function action_index(){
		;
	}
	public function check_access($function_access_lvl=0){
		//Zwraca TRUE dla:
		//0 - wszystkich
		//1 - zalogowanych
		//-1 - niezalogowanych
		//10+lvl określony lvl
		if ($function_access_lvl==-1){
			if ($this->user_id===NULL){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else if ($function_access_lvl==0){
			//echo 'chuj';
			return TRUE;
		}
		else if ($function_access_lvl==1){
			if ($this->user_id!==NULL){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else if ($function_access_lvl>10){
			if ($this->user_access_lvl>=$function_access_lvl){
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	public function create_menu(){
		
		$menu = array(	'/' => "HOME",
						'/u/1' => "Kevins profile",
						'/user/register' => "Create Profile"
					);
		if ($this->check_access(1)){
			//jeśli zalogowany
			$menu['/user/'] = "Kokpit";
			$menu['/u/'.$this->user_id] = "Twój profil";
		}
		
		
		return $menu;
	}
	
	function create_logon(){
		if ($this->check_access(-1)){
			//jeśli niezalogowany
			return View::factory('user/login');
		}
		else if ($this->check_access(1)){
			$ainside = $this->session->get('user_name').' '.$this->session->get('user_surname');
			return HTML::anchor('/u/'.$this->user_id, $ainside).' <br />'.HTML::anchor('/user/logout','Wyloguj się!');
		}
	}
	
	public function after(){
		//dzieje się po wczytaniu kontrolera
		$this->view->menu = View::factory('default/menu');
		$this->view->menu->menu_items = $this->create_menu();
		$this->view->logon = $this->create_logon();
		//$this->view->menu->loggon = $loggon;
		$this->response->body($this->view->render());
		parent::after();
	}
	
	
}
?>
