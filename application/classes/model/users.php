<?php
/*
 * users.php
 * 
 * Copyright 2012 Kevin Wolny <kevin.wolny@gmail.com>
 * 
 */
class Model_Users extends Model{
	protected $database;
	
	/*public function __construct(){
		//nie ma sensu tutaj łączyć, łączy się samo jeśli da rade po defaulcie
		$this->database = Database::instance('default12');
	}*/

	public function get_preview($id){
		//echo 'x'.$id.'x';
		//$db = Database::instance();
		//echo Database::SELECT;
		$query = DB::query(1,'SELECT * FROM `users` where `id`=:id LIMIT 0,1');
		$query->param(':id',$id);
		
		//echo Debug::vars((string) $query);
		$result = $query->execute();
		if (count($result)>0){
			return $result[0];
		}
		else{
			return 0;
		}
		//unset(Database::$instances['pdo']);
	}
	
	protected function make_hash($string, $mail){
		$salt=$mail[0].$mail[1].$mail[2];
		return hash('sha512', $string.'x'.$salt);
	}
	
	public function register_user($reg){
		$unique_q = DB::query(1,'SELECT COUNT(*) AS `C` FROM `users` WHERE `mail`=:mail');
		$unique_q->param(':mail', $reg['mail']);
		$unique_r = $unique_q->execute();
		if ($unique_r[0]['C']=="0"){
			$pwd = Model_Users::make_hash($reg['pwd'],$reg['mail']);
			$insert_q = DB::query(2,'INSERT INTO `users` (`id`, `pwd`, `mail`, `name`, `surname`, `created`, `joined`, `about`, `hobbies`, `points_all`, `points_season`, `krzyz`, `naramiennik`, `stop_instr`, `stop_harc`, `odznaka_kszt`, `odznaka_prog`) VALUES (NULL, :pwd, :mail, :name, :surname, NOW(), "", "", "", "", "", "", "", "", "", "", "");');
			$insert_q->param(':mail', $reg['mail']);
			$insert_q->param(':name', $reg['name']);
			$insert_q->param(':surname', $reg['surname']);
			$insert_q->param(':pwd', $pwd);
			list($last_id, $rows) = $insert_q->execute();
			if ($rows==1){
				return 1;	
			}
			else{
				return 'Błąd podczas zapisywania do bazy ('.$last_id.';'.$rows.');';
			}
		}
		else{
			return 'Na ten mail jest już zarejestrowane konto';
		}
	}
	
	public function login_user($mail, $pwd){
		$pwd = Model_Users::make_hash($pwd, $mail);
		$select_q = DB::query(1, 'SELECT `id`, `name`, `surname` FROM `users` WHERE `mail`=:mail AND `pwd`=:pwd');
		$select_q->param(':mail', $mail);
		$select_q->param(':pwd', $pwd);
		$select_r = $select_q->execute();
		//var_dump($select_r);
		if ($select_r[0]['id']!=0){
			return array("id" => $select_r[0]['id'],'name' => $select_r[0]['name'],"surname" => $select_r[0]['surname']);
		}
		else{
			return 0;
		}
	}
	
	public function get_stream($id_autor, $start=0, $limit=10){
		$select_q = DB::query(1, 'SELECT `id`, `text`,`date`, `children_amount`  FROM `stream` WHERE `id_autor`=:id_autor and `status`=1  and `id_parent`= 0 ORDER BY `date` DESC');
		$select_q->param(':id_autor', $id_autor);
		$select_r = $select_q->execute();
		//var_dump($select_r);
		return $select_r;
	}
	
	public function get_tweet($id){
		//$db = Database::instance();
		//stare zapytanie
		//$select_q = DB::query(1, 'SELECT `id_autor`, `id_parent`, `text`, `date`, `children_amount` FROM `stream` WHERE `id`=:id and `status`=1 ORDER BY `date` DESC');
		$select_q = DB::query(1, 'SELECT `stream`.`id_autor`, `stream`.`id_parent`, `stream`.`text`, `date`, `stream`.`children_amount`, `users`.`name`, `users`.`surname`  FROM `stream`, `users` WHERE `stream`.`id_autor`=`users`.`id` AND `stream`.`id`=:id AND `stream`.`status`=1 ORDER BY `stream`.`date` DESC');
		
		//var_dump($select_q);
		$select_q->param(':id',$id);
		$select_r = $select_q->execute();
		//var_dump($select_r);
		if ($select_r[0]['id_parent']!=0){//jeśli chcemy się wbić w komentarz co nie jest dobre, zwracamy inta, a w controlerze robimy redirecta
			//echo $select_r[0]['id_parent'];
			return (int)($select_r[0]['id_parent']);
		}
		else{
			//var_dump($select_r[0]);
			$data = $select_r[0];
			if ($data['children_amount']>0){//jeśli są jakieś komentarze do wpisu to pobieramy
				$select_q = DB::query(1, 'SELECT `id`, `id_autor`, `text`,`date` FROM `stream` WHERE `status`=1  and `id_parent`=:id ORDER BY `date` ASC');
				$select_q->param(':id',$id);
				$select_r = $select_q->execute();
				for ($i = 0; $i < $data['children_amount']; $i++){
					//echo $select_r[$i]['id_autor'].' ';
					$autors[$select_r[$i]['id_autor']]=1;
				}
				//$id_autor=0;
				$autors_q = DB::query(1,'SELECT `id`, `name`, `surname` FROM `users` WHERE `id`=:id')
					->bind(':id', $id_autor);
				foreach ($autors as $id_autor => $value){
					//echo $id_autor.' '.$value.'x';
					$tmp = $autors_q->execute();
					$autors_r[$id_autor] = $tmp[0];
				}
				for ($i = 0; $i < $data['children_amount']; $i++){
					$data['tweets'][$i] = $select_r[$i];
					//echo $data['tweets'][$i]['id_autor'];
					$data['tweets'][$i]['autor_name'] = $autors_r[$data['tweets'][$i]['id_autor']]['name'];
					$data['tweets'][$i]['autor_surname'] = $autors_r[$data['tweets'][$i]['id_autor']]['surname'];
					$data['tweets'][$i]['autor_id'] = $autors_r[$data['tweets'][$i]['id_autor']]['id'];
				}
				//var_dump($data['tweets']);
				//return $data;
			}
			return $data;
		}
	}
	
	public function add_tweet($tweet){
		//var_dump($tweet);
		$insert_q = DB::query(2,"INSERT INTO `stella`.`stream` (`id`, `id_autor`, `id_parent`, `status`, `text`, `date`, `children_amount`) VALUES (NULL, :user_id, '0', '1', :text, NOW(), '0')");
		$insert_q->param(':user_id', $tweet['id_autor']);
		$insert_q->param(':text', $tweet['text']);
		list($last_id,$rows) = $insert_q->execute();
		if ($rows==1){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>
