<?php
/*
 * menu.php
 * 
 * Copyright 2012 Kevin Wolny <kevin.wolny@gmail.com>
 * 
 */
echo '<ul class="link-list">';
foreach	($menu_items as $href => $name){
	echo '<li>'.HTML::anchor($href,$name).'</li>';
	//echo '<br/>';
}
echo '</ul>';
/*
echo HTML::anchor('/','Home Page').'<br />';
echo HTML::anchor('/u/1','Przykładowy profil').'<br />';
echo HTML::anchor('/user/register/', 'Create Account').'<br />';
echo HTML::anchor('/user/login/', 'Log in').'<br />';
echo HTML::anchor('/user/logout/', 'Log Out').'<br />';
echo HTML::anchor('/user/', 'Kokpit').'<br />';
echo $loggon;
* */
?>

