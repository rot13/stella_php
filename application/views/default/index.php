<?php
/*
 * index.php
 * 
 * Copyright 2012 Kevin Wolny <kevin.wolny@gmail.com>
 * 
 */

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" class="no-js">
<head>
	<title><?php echo $title?></title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
	<meta name="generator" content="Geany 0.22" />	
<?php foreach ($styles as $file) echo '	'.HTML::style($file).PHP_EOL ?>
<?php foreach ($scripts as $file) echo '	'.HTML::script($file).PHP_EOL ?>
</head>
<body>
	<?php
/*<div id="top">
	<div id="NAGLOWEK">Stella</div>
	<div id="MENU"><?php echo $menu ?></div>
	<div id="TRESC"><?php echo $body ?></div>
	<div id="STOPKA"><?php echo $snv ?></div>
</div>*/
?>


<div class="row">
	<div class="eight columns">
	<div id="logo" ><p>Logo</p></div>
	</div>
	<div class="four columns">
	<p class="right"><?php echo $logon ?></p>
	</div>
</div>
<div class="row">
	<div class="eight columns">
	<div class="panel"><?php echo $menu ?> </div>
	</div>
	<div class="four columns">
	<div class="panel" id="szukajka">Szukajka</div>
	</div>
</div>
<div class="row">
	<div class="eight columns">
	<p><?php echo $body ?></p>
	</div>
	<div class="four columns">
	<p>Prawe pole</p>
	</div>
</div>
<div class="row">
	<div class="twelve columns">
	<p><?php echo $snv ?></p>
	</div>
</div>

  <!-- Included JS Files (Compressed) -->
  <script src="javascripts/foundation.min.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>
</body>

</html>
